var roleHarvester = require('role.harvester');
var roleUpgrader = require('role.upgrader');
var roleBuilder = require('role.builder');
var roleRepairer = require("role.repairer");
var roleContainerManager = require("role.containerManager");

var handlerTower = require("handler.tower");

Memory["harvesting"]={};
for (var sourse of Game.getObjectById("5e95ddf46687f68e77fbc12d").room.find(FIND_SOURCES)){
    Memory["harvesting"][sourse.id]=[0,3];
}

Memory["creepN"]=0;

module.exports.loop = function () {
    
    require('main_spawn').run();
//  require('main.spawn').run();
    for(var name in Game.structures){
        var cur_structure = Game.structures[name];
        if (cur_structure.structureType==STRUCTURE_TOWER)
            handlerTower.run(cur_structure);
    }
    
    for(var name in Game.creeps) {
        var creep = Game.creeps[name];
        // console.log(creep)
        if(creep.memory.role == 'harvester') {
            roleHarvester.run(creep);
        }
        if(creep.memory.role == 'upgrader') {
            roleUpgrader.run(creep);
        }
        if(creep.memory.role == 'builder') {
            roleBuilder.run(creep);
        }
        if(creep.memory.role == 'repairer') {
            roleRepairer.run(creep);
        }
        if(creep.memory.role == 'containerManager') {
            roleContainerManager.run(creep);
        }
    }
}
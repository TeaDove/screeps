module.exports = {
    run(creep){
        var sources = creep.pos.findClosestByPath(FIND_SOURCES, {
                    filter: (source) => {
                        return (source.energy>0);}});
        if (creep.harvest(sources)==ERR_NOT_IN_RANGE){
            creep.moveTo(sources,{visualizePathStyle: {stroke: '#FFAA00'}});
        }
    }
};
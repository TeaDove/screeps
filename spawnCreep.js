module.exports = {
    run(body, role){
        var newName = role+" ";
        for (var item of body)
            newName+=item[0];
        newName+=" "+Memory['creepN'];
        Memory['creepN']+=1;
        Game.spawns['Spawn1'].spawnCreep(body, newName, 
            {memory: {role: role}});
    }
};